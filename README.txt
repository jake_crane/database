This program was written in Spring 2012.

This program simulates a basic database.
Users can add, delete, browse and search records using a CLI.
Records are written to and read from a file.