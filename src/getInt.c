#include <stdio.h>
#include <string.h>

int getInt() {

	int newInt;
	char input[12];
	fgets(input, sizeof(input), stdin);
	sscanf(input, "%d", &newInt);

	if (input[strlen(input) - 1] != '\n') {
		int ch;
		while ((ch = fgetc(stdin)) != EOF) {
			if (ch == '\n') {
				break;
			}
		}
	}

	return newInt;
}
