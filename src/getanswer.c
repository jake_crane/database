#include <stdio.h>
#include <ctype.h>

char getAnswer(char *question) {
	char answer = -1;
	printf("%s (y or n) ", question);
	fflush(stdout);
	while ((answer = fgetc(stdin)) != EOF) {
		fflush(stdout);
		answer = tolower(answer);
		if (answer == 'y' || answer == 'n') {
			break;
		}
		printf("%s (y or n) ", question);
		fflush(stdout);
	}

	int ch;
	while ((ch = fgetc(stdin)) != EOF) {
		if (ch == '\n') {
			break;
		}
	}

	return answer;
}
