typedef struct entry_s record;
struct entry_s {
	char name[32];
	long long telephone;
	char address[32];
	record *next;
	record *previous;
};

void stringToLower(char* string);
int stringContainsStringIgnoreCase(char *string1, char *string2);
char getAnswer(char *question);
int getChar();
int getInt();
void clearScreen();
