#include <stdio.h>
#include <string.h>

int getChar() {
	char newChar;
	char input[3];
	fgets(input, sizeof(input), stdin);
	sscanf(input, "%c", &newChar);

	if (input[strlen(input) - 1] != '\n') {
		int ch;
		while ((ch = fgetc(stdin)) != EOF) {
			if (ch == '\n') {
				break;
			}
		}
	}

	return newChar;
}
