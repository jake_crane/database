/*****************************************************************************\
*                                                                             *
* Author:               Jake Crane                                            *
* UserID:               JAC86520                                              *
* Date:                 12/3/2012                                             *
* Project ID:           Chapter 6 Exercise 12                                 *
* CS class:             CS 2500                                               *
* Problem Description: This program manages a database.                       *
*                                                                             *
*                                                                             *
\*****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <ctype.h>
#include "Database.h"

#define MAXUMIM_LINE_LENGTH 73
#define NAME_SEARCH 1
#define TELEPHONE_SEARCH 2
#define ADDRESS_SEARCH 3

void printRecord(record *currentRecord) {
	printf("\nName: %s\n", (*currentRecord).name);
	printf("Telephone: %lld\n", (*currentRecord).telephone);
	printf("Address: %s\n", (*currentRecord).address);
	printf("\n");
}

void printDatabase(record *firstRecord) {
	record *currentRecord = firstRecord;
	while (currentRecord != NULL){
		printRecord(currentRecord);
		currentRecord = currentRecord->next;
	}
}

void readLine(char *line, record *node) {
	char telephone[11];
	int linechar = 0;

	int lineLength = strlen(line);

	int i = 0;
	for (; linechar < lineLength && line[linechar] != '|'; linechar++, i++) {
		(*node).name[i] = line[linechar];
	}
	(*node).name[i] = 0;

	linechar++;
	i = 0;
	for (; linechar < lineLength && line[linechar] != '|'; linechar++, i++) {
		telephone[i] = line[linechar];
	}
	telephone[i] = 0;
	(*node).telephone = atoll(telephone);

	linechar++;
	i = 0;
	for (; linechar < lineLength && line[linechar] != '\n'; linechar++, i++) {
		(*node).address[i] = line[linechar];
	}
	(*node).address[i] = 0;
}

void ReadDatabase(FILE *database, record **firstRecord) {
	char line[MAXUMIM_LINE_LENGTH];
	record *currentRecord;
	record *previousrecord;

	// read all records from the input file into the linked list
	while(fgets(line, sizeof(line), database) != NULL) {
		currentRecord = (record *) malloc(sizeof(record));
		if (currentRecord == NULL){
			printf("Out of memory\n");
			exit(EXIT_FAILURE);
		}
		readLine(line, currentRecord);
		currentRecord->next = NULL;
		if (*firstRecord == NULL) {
			*firstRecord = currentRecord;
			(*firstRecord)->previous = NULL;
		} else {
			previousrecord->next = currentRecord;
			currentRecord->previous = previousrecord;
		}
		previousrecord = currentRecord;
	}
	fclose(database);
}

int writeDatabase(FILE *database, record *firstRecord) {

	record *currentRecord = firstRecord;

	if (database == NULL) {
		perror("failed to open database");
		return -1;
	}

	while (currentRecord != NULL) {
		fprintf(database, "%s|%lld|%s\n", currentRecord->name, currentRecord->telephone, currentRecord->address);
		currentRecord = currentRecord->next;
	}

	fclose(database);

	return 0;

}

void freeList(record *firstRecord) {

	record *currentRecord = firstRecord;

	record *nodeToDelete;

	while (currentRecord != NULL) {
		nodeToDelete = currentRecord;
		currentRecord = currentRecord->next;
		free(nodeToDelete);
	}

}

int createAndAddRecord(record **firstRecord) {

	record *newrecord = (record *) malloc(sizeof(record));
	if (newrecord == NULL){
		return 0;
	}

	printf("Enter a name for the new record: ");
	fflush(stdout);
	fgets(newrecord->name, sizeof(newrecord->name), stdin);
	newrecord->name[strlen(newrecord->name) - 1] = 0;

	printf("Enter a phone number for the new record: ");
	fflush(stdout);
	char input[16];
	fgets(input, sizeof(input), stdin);
	sscanf(input, "%lld", &newrecord->telephone);

	printf("Enter an address for the new record: ");
	fflush(stdout);
	fgets(newrecord->address, sizeof(newrecord->address), stdin);
	newrecord->address[strlen(newrecord->address) - 1] = 0;

	if (*firstRecord == NULL) {
		*firstRecord = newrecord;
		(*firstRecord)->next = NULL;
		(*firstRecord)->previous = NULL;
	} else {
		record *currentRecord = *firstRecord;
		while (currentRecord->next != NULL) {
			currentRecord = currentRecord->next;
		}
		newrecord->previous = currentRecord;
		newrecord->next = NULL;
		currentRecord->next = newrecord;
	}

	FILE *database = fopen("Database", "w+");
	writeDatabase(database, *firstRecord);

	return 1;
}

int stringContainsStringIgnoreCase(char *string1, char *string2) {
	char lowercaseString1[32];
	strcpy(lowercaseString1, string1);
	stringToLower(lowercaseString1);
	stringToLower(string2);

	if (strstr(lowercaseString1, string2) != NULL) {
		return 1;
	}

	return 0;
}

void searchAndPrint(record *firstRecord, char *searchString, int searchType) {
	int count = 0;
	record *currentRecord = firstRecord;

	char telephoneNumberString[16];
	while (currentRecord != NULL) {
		switch (searchType) {
		case NAME_SEARCH:
			if (stringContainsStringIgnoreCase(currentRecord->name, searchString)) {
				count++;
				printRecord(currentRecord);
			}
			break;
		case TELEPHONE_SEARCH:
			sprintf(telephoneNumberString, "%lld", (*currentRecord).telephone);
			if (stringContainsStringIgnoreCase(telephoneNumberString, searchString)) {
				count++;
				printRecord(currentRecord);
			}
			break;
		case ADDRESS_SEARCH:
			if (stringContainsStringIgnoreCase(currentRecord->address, searchString)) {
				count++;
				printRecord(currentRecord);
			}
			break;
		}
		currentRecord = currentRecord->next;
	}

	printf("%d matches found.\n\n\n", count);
}

void manageSearch(record *firstRecord) {

	printf("Search Menu:\n");
	printf("1.Search Names\n");
	printf("2.Search Telephone Numbers\n");
	printf("3.Search Addresses\n");
	printf("Choose An Option: ");
	fflush(stdout);

	int MenuChoice = -1;
	char input[3];
	char searchString[MAXUMIM_LINE_LENGTH];


	while (MenuChoice != NAME_SEARCH
			&& MenuChoice != TELEPHONE_SEARCH
			&& MenuChoice != ADDRESS_SEARCH) {
		fgets(input, sizeof(input), stdin);
		sscanf(input, "%d", &MenuChoice);
	}

	clearScreen();

	char *messages[] = {"Enter a name to search for: ", "Enter a telephone number to search for: ", "Enter a address to search for: "};
	printf("%s", messages[MenuChoice -1]);
	fflush(stdout);
	fgets(searchString, sizeof(searchString), stdin);
	searchString[strlen(searchString) - 1] = 0;
	searchAndPrint(firstRecord, searchString, MenuChoice);
}

void deleteRecord(record **recordToDelete) {
	if ((*recordToDelete)->previous == NULL) { //first record
		if ((*recordToDelete)->next == NULL) {
			free(*recordToDelete);
			*recordToDelete = NULL;
			return;
		} else {
			(*recordToDelete)->next->previous = NULL;
		}
	} else if ((*recordToDelete)->next == NULL) { //last record
		(*recordToDelete)->previous->next = NULL;
	} else { //not first or last record
		(*recordToDelete)->previous->next = (*recordToDelete)->next;
		(*recordToDelete)->next->previous = (*recordToDelete)->previous;

	}
	printf("%s removed\n", (*recordToDelete)->name);
	fflush(stdout);
	free(*recordToDelete);

}

void browseRecords(record **firstrecord) {
	record *currentRecord = *firstrecord;
	printRecord(currentRecord);

	int browsingRecords = 1;
	int MenuChoice;

	while (browsingRecords) {
		printf("\nMain Menu:\n");
		printf("1.Next Record\n");
		printf("2.Previous Record\n");
		printf("3.Delete Record\n");
		printf("4.Main Menu\n");
		printf("Choose An Option: ");
		fflush(stdout);

		MenuChoice = getInt();

		switch(MenuChoice) {
		case 1:
			clearScreen();
			if (currentRecord->next != NULL) {
				currentRecord = currentRecord->next;
			} else {
				printf("Last record.\n");
			}
			printRecord(currentRecord);
			break;
		case 2:
			clearScreen();
			if (currentRecord->previous != NULL) {
				currentRecord = currentRecord->previous;
			} else {
				printf("First record.\n");
			}
			printRecord(currentRecord);
			break;
		case 3:
			if (*firstrecord == NULL) {
				printf("No entries to delete.\n");
				break;
			}
			if (getAnswer("Are you sure you want to delete this record?") == 'y') {
				if (currentRecord == *firstrecord) {
					*firstrecord = (*firstrecord)->next;
				}
				clearScreen();
				if (currentRecord == *firstrecord) {
					deleteRecord(firstrecord);
				} else {
					deleteRecord(&currentRecord);
				}

				FILE *database = fopen("Database", "w+");
				writeDatabase(database, *firstrecord);

				currentRecord = *firstrecord;
				if (*firstrecord == NULL) {
					return;
				}
				printRecord(currentRecord);
			}
			break;
		case 4:
			browsingRecords = 0;
			clearScreen();
			break;
		default:
			clearScreen();
			printf("Invalid menu choice\n\n");
			break;
		}
	}
}

int main(void) {
	int programShouldRun = 1;
	int MenuChoice;

	record *firstRecord = NULL;
	FILE *database = fopen("Database", "r");


	if (database != NULL ) {
		ReadDatabase(database, &firstRecord);
	}

	clearScreen();

	while (programShouldRun) {
		printf("Main Menu:\n");
		printf("1.Add Record\n");
		printf("2.Browse Records\n");
		printf("3.Search Records\n");
		printf("4.Quit\n");
		printf("Choose An Option: ");
		fflush(stdout);

		char input[3];
		fgets(input, sizeof(input), stdin);
		sscanf(input, "%d", &MenuChoice);

		switch(MenuChoice) {
		case 1:
			clearScreen();
			createAndAddRecord(&firstRecord);
			fflush(stdout);
			break;
		case 2:
			clearScreen();
			if (firstRecord != NULL) {
				browseRecords(&firstRecord);
			} else {
				printf("No Records Found. Unable to browse.\n\n");
				fflush(stdout);
			}
			break;
		case 3:
			clearScreen();
			if (firstRecord != NULL) {
				manageSearch(firstRecord);
			} else {
				printf("No Records Found. Unable to search.\n\n");
				fflush(stdout);
			}
			break;
		case 4:
			printf("Quitting\n");
			programShouldRun = 0;
			break;
		default:
			clearScreen();
			printf("Invalid menu choice\n\n");
			break;
		}
	}

	database = fopen("Database", "w+");
	writeDatabase(database, firstRecord);

	freeList(firstRecord);

	return EXIT_SUCCESS;
}
