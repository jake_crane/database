#include <stdlib.h>

void clearScreen() {

	#ifdef _WIN64
		system("cls");
	#elif _WIN32
		system("cls");
	#else
		system("tput clear");
	#endif

}
